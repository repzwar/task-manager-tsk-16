package ru.pisarev.tm.api.controller;

import ru.pisarev.tm.model.Project;

public interface IProjectController {

    void showList();

    void showById();

    void showByIndex();

    void showByName();

    void create();

    Project add(String name, String description);

    void clear();

    void removeById();

    void removeByIndex();

    void removeByName();

    void updateByIndex();

    void updateById();

    void startById();

    void startByIndex();

    void startByName();

    void finishById();

    void finishByIndex();

    void finishByName();

}
