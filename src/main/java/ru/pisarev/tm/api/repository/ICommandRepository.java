package ru.pisarev.tm.api.repository;

import ru.pisarev.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}