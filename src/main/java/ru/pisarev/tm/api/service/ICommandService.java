package ru.pisarev.tm.api.service;

import ru.pisarev.tm.model.Command;

public interface ICommandService {
    Command[] getTerminalCommands();
}
