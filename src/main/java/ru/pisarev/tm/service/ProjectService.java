package ru.pisarev.tm.service;

import ru.pisarev.tm.api.repository.IProjectRepository;
import ru.pisarev.tm.api.service.IProjectService;
import ru.pisarev.tm.enumerated.Status;
import ru.pisarev.tm.exception.empty.EmptyIdException;
import ru.pisarev.tm.exception.empty.EmptyIndexException;
import ru.pisarev.tm.exception.empty.EmptyNameException;
import ru.pisarev.tm.exception.system.IndexIncorrectException;
import ru.pisarev.tm.model.Project;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public List<Project> findAll(final Comparator<Project> comparator) {
        if (comparator == null) return null;
        return projectRepository.findAll(comparator);
    }

    @Override
    public Project findById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findById(id);
    }

    @Override
    public Project findByIndex(final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (index > projectRepository.getSize()) throw new IndexIncorrectException();
        return projectRepository.findByIndex(index);
    }

    @Override
    public Project findByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findByName(name);
    }

    @Override
    public Project add(final Project project) {
        if (project == null) return null;
        projectRepository.add(project);
        return project;
    }

    @Override
    public void remove(final Project project) {
        if (project == null) return;
        projectRepository.remove(project);
    }

    @Override
    public Project removeById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.removeById(id);
    }

    @Override
    public Project removeByIndex(final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return projectRepository.removeByIndex(index);
    }

    @Override
    public Project removeByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.removeByName(name);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Project updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = projectRepository.findById(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = projectRepository.findByIndex(index);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project startById(String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Project project = projectRepository.findById(id);
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project startByIndex(Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Project project = projectRepository.findByIndex(index);
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project startByName(String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = projectRepository.findByName(name);
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project finishById(String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Project project = projectRepository.findById(id);
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        return project;
    }

    @Override
    public Project finishByIndex(Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Project project = projectRepository.findByIndex(index);
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        return project;
    }

    @Override
    public Project finishByName(String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = projectRepository.findByName(name);
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        return project;
    }

}
