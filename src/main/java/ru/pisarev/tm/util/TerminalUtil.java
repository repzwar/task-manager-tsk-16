package ru.pisarev.tm.util;

import ru.pisarev.tm.exception.AbstractException;
import ru.pisarev.tm.exception.system.IndexIncorrectException;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() throws AbstractException {
        final String value = SCANNER.nextLine();
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            throw new IndexIncorrectException(value);
        }
    }

    static void incorrectValue() {
        System.out.println("Incorrect values");
    }

}
