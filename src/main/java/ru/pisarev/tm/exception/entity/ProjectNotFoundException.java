package ru.pisarev.tm.exception.entity;

import ru.pisarev.tm.exception.AbstractException;

public class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Error. Project not found.");
    }

}
