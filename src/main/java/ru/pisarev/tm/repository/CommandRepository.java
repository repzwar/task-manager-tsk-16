package ru.pisarev.tm.repository;

import ru.pisarev.tm.api.repository.ICommandRepository;
import ru.pisarev.tm.constant.ArgumentConst;
import ru.pisarev.tm.constant.TerminalConst;
import ru.pisarev.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command HELP = new Command(
            TerminalConst.CMD_HELP, ArgumentConst.ARG_HELP, "Display list of terminal commands."
    );

    private static final Command VERSION = new Command(
            TerminalConst.CMD_VERSION, ArgumentConst.ARG_VERSION, "Display program version."
    );

    private static final Command ABOUT = new Command(
            TerminalConst.CMD_ABOUT, ArgumentConst.ARG_ABOUT, "Display developer info."
    );

    private static final Command EXIT = new Command(
            TerminalConst.CMD_EXIT, null, "Close application."
    );

    private static final Command INFO = new Command(
            TerminalConst.CMD_INFO, ArgumentConst.ARG_INFO, "Display system info."
    );

    private static final Command COMMANDS = new Command(
            TerminalConst.CMD_COMMANDS, null, "Show program commands."
    );

    private static final Command ARGUMENTS = new Command(
            TerminalConst.CMD_ARGUMENTS, null, "Show program arguments."
    );

    private static final Command TASK_CREATE = new Command(
            TerminalConst.CMD_TASK_CREATE, null, "Create task."
    );

    private static final Command TASK_CLEAR = new Command(
            TerminalConst.CMD_TASK_CLEAR, null, "Delete all tasks."
    );

    private static final Command TASK_LIST = new Command(
            TerminalConst.CMD_TASK_LIST, null, "Show all tasks."
    );

    private static final Command TASK_SHOW_BY_ID = new Command(
            TerminalConst.CMD_TASK_SHOW_BY_ID, null, "Show task by id."
    );

    private static final Command TASK_SHOW_BY_INDEX = new Command(
            TerminalConst.CMD_TASK_SHOW_BY_INDEX, null, "Show task by index."
    );

    private static final Command TASK_SHOW_BY_NAME = new Command(
            TerminalConst.CMD_TASK_SHOW_BY_NAME, null, "Show task by name."
    );

    private static final Command TASK_REMOVE_BY_ID = new Command(
            TerminalConst.CMD_TASK_REMOVE_BY_ID, null, "Remove task by id."
    );

    private static final Command CMD_TASK_REMOVE_BY_INDEX = new Command(
            TerminalConst.CMD_TASK_REMOVE_BY_INDEX, null, "Remove task by index."
    );

    private static final Command TASK_REMOVE_BY_NAME = new Command(
            TerminalConst.CMD_TASK_REMOVE_BY_NAME, null, "Remove task by name."
    );

    private static final Command TASK_UPDATE_BY_ID = new Command(
            TerminalConst.CMD_TASK_UPDATE_BY_ID, null, "Update task by id."
    );

    private static final Command TASK_UPDATE_BY_INDEX = new Command(
            TerminalConst.CMD_TASK_UPDATE_BY_INDEX, null, "Update task by index."
    );

    private static final Command TASK_START_BY_ID = new Command(
            TerminalConst.CMD_TASK_START_BY_ID, null, "Start task by id."
    );

    private static final Command CMD_TASK_START_BY_INDEX = new Command(
            TerminalConst.CMD_TASK_START_BY_INDEX, null, "Start task by index."
    );

    private static final Command TASK_START_BY_NAME = new Command(
            TerminalConst.CMD_TASK_START_BY_NAME, null, "Start task by name."
    );

    private static final Command TASK_FINISH_BY_ID = new Command(
            TerminalConst.CMD_TASK_FINISH_BY_ID, null, "Finish task by id."
    );

    private static final Command CMD_TASK_FINISH_BY_INDEX = new Command(
            TerminalConst.CMD_TASK_FINISH_BY_INDEX, null, "Finish task by index."
    );

    private static final Command TASK_FINISH_BY_NAME = new Command(
            TerminalConst.CMD_TASK_FINISH_BY_NAME, null, "Finish task by name."
    );

    private static final Command TASK_BIND_BY_ID = new Command(
            TerminalConst.CMD_TASK_BIND_BY_ID, null, "Bind task to project."
    );

    private static final Command TASK_UNBIND_BY_ID = new Command(
            TerminalConst.CMD_TASK_UNBIND_BY_ID, null, "Unbind task from project."
    );

    private static final Command TASK_FIND_BY_PROJECT_ID = new Command(
            TerminalConst.CMD_TASK_FIND_BY_PROJECT_ID, null, "Show all task in project by project id."
    );

    private static final Command PROJECT_CREATE = new Command(
            TerminalConst.CMD_PROJECT_CREATE, null, "Create project."
    );

    private static final Command PROJECT_CLEAR = new Command(
            TerminalConst.CMD_PROJECT_CLEAR, null, "Delete all projects."
    );

    private static final Command PROJECT_LIST = new Command(
            TerminalConst.CMD_PROJECT_LIST, null, "Show all projects."
    );

    private static final Command PROJECT_SHOW_BY_ID = new Command(
            TerminalConst.CMD_PROJECT_SHOW_BY_ID, null, "Show project by id."
    );

    private static final Command PROJECT_SHOW_BY_INDEX = new Command(
            TerminalConst.CMD_PROJECT_SHOW_BY_INDEX, null, "Show project by index."
    );

    private static final Command PROJECT_SHOW_BY_NAME = new Command(
            TerminalConst.CMD_PROJECT_SHOW_BY_NAME, null, "Show project by name."
    );

    private static final Command PROJECT_REMOVE_BY_ID = new Command(
            TerminalConst.CMD_PROJECT_REMOVE_BY_ID, null, "Remove project by id."
    );

    private static final Command CMD_PROJECT_REMOVE_BY_INDEX = new Command(
            TerminalConst.CMD_PROJECT_REMOVE_BY_INDEX, null, "Remove project by index."
    );

    private static final Command PROJECT_REMOVE_BY_NAME = new Command(
            TerminalConst.CMD_PROJECT_REMOVE_BY_NAME, null, "Remove project by name."
    );

    private static final Command PROJECT_UPDATE_BY_ID = new Command(
            TerminalConst.CMD_PROJECT_UPDATE_BY_ID, null, "Update project by id."
    );

    private static final Command PROJECT_UPDATE_BY_INDEX = new Command(
            TerminalConst.CMD_PROJECT_UPDATE_BY_INDEX, null, "Update project by index."
    );

    private static final Command PROJECT_START_BY_ID = new Command(
            TerminalConst.CMD_PROJECT_START_BY_ID, null, "Start project by id."
    );

    private static final Command CMD_PROJECT_START_BY_INDEX = new Command(
            TerminalConst.CMD_PROJECT_START_BY_INDEX, null, "Start project by index."
    );

    private static final Command PROJECT_START_BY_NAME = new Command(
            TerminalConst.CMD_PROJECT_START_BY_NAME, null, "Start project by name."
    );

    private static final Command PROJECT_FINISH_BY_ID = new Command(
            TerminalConst.CMD_PROJECT_FINISH_BY_ID, null, "Finish project by id."
    );

    private static final Command CMD_PROJECT_FINISH_BY_INDEX = new Command(
            TerminalConst.CMD_PROJECT_FINISH_BY_INDEX, null, "Finish project by index."
    );

    private static final Command PROJECT_FINISH_BY_NAME = new Command(
            TerminalConst.CMD_PROJECT_FINISH_BY_NAME, null, "Finish project by name."
    );

    private static final Command[] TERMINAL_COMMANDS = new Command[]{
            HELP, VERSION, ABOUT, INFO, COMMANDS, ARGUMENTS,
            TASK_CREATE, TASK_CLEAR, TASK_LIST,
            TASK_SHOW_BY_ID, TASK_SHOW_BY_INDEX, TASK_SHOW_BY_NAME,
            TASK_REMOVE_BY_ID, CMD_TASK_REMOVE_BY_INDEX, TASK_REMOVE_BY_NAME,
            TASK_UPDATE_BY_ID, TASK_UPDATE_BY_INDEX,
            TASK_START_BY_ID, CMD_TASK_START_BY_INDEX, TASK_START_BY_NAME,
            TASK_FINISH_BY_ID, CMD_TASK_FINISH_BY_INDEX, TASK_FINISH_BY_NAME,
            TASK_BIND_BY_ID, TASK_UNBIND_BY_ID, TASK_FIND_BY_PROJECT_ID,
            PROJECT_CREATE, PROJECT_CLEAR, PROJECT_LIST,
            PROJECT_SHOW_BY_ID, PROJECT_SHOW_BY_INDEX, PROJECT_SHOW_BY_NAME,
            PROJECT_REMOVE_BY_ID, CMD_PROJECT_REMOVE_BY_INDEX, PROJECT_REMOVE_BY_NAME,
            PROJECT_UPDATE_BY_ID, PROJECT_UPDATE_BY_INDEX,
            PROJECT_START_BY_ID, CMD_PROJECT_START_BY_INDEX, PROJECT_START_BY_NAME,
            PROJECT_FINISH_BY_ID, CMD_PROJECT_FINISH_BY_INDEX, PROJECT_FINISH_BY_NAME,
            EXIT
    };

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
